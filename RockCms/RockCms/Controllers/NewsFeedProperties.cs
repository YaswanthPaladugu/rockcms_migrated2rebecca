﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.interfaces;


namespace UmbracoCms.Controllers 
{
    public class NewsFeedProperties : IProperty
    {
        public NewsFeedProperties()
        {
            image = "";
            pgid = 0;
            name = "";
            contentTag = "";
            publishername = "";
            publishdate = "";
            bodyText = "";
            Alias = "";
            Value = "";
        }
        public string image { get; set; }
        public int pgid { get; set; }

        public string name { get; set; }

        public string contentTag { get; set; }


        public string publishername { get; set; }

        public string publishdate { get; set; }

        public string bodyText { get; set; }

        public string Alias { get; set; }

        public string Value { get; set; }
  
    }
}