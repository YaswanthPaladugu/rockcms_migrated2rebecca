﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.interfaces;

namespace UmbracoCms.Controllers
{
    public class ImpLinksProperties : IProperty
    {
        public ImpLinksProperties()
        {
            Alias = "";
            link = "";
            Value = "";
            pgid = 0;
            name = "";
        }

        public int pgid { get; set; }
        public string name { get; set; }

        public string link { get; set; }
        public string Alias {get; set; }
  
        public string Value { get; set; }
    }
}