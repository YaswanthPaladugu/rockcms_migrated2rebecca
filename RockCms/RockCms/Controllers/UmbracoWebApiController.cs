﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Cors;
using umbraco.interfaces;
using umbraco.NodeFactory;
using Umbraco.Web.WebApi;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Configuration;


namespace UmbracoCms.Controllers
{
    public class UmbracoWebApiController : UmbracoApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]

        [AcceptVerbs("Options")]
        [HttpGet]
        public HttpResponseMessage Get(string id = null)
        {
            int Id = Convert.ToInt32(id);

            Node node = null;
            List<GetNewsNodes> viewNode;

            if (Id > 0 || id != null)
            {
                node = new Node(Id);
                if (node == null)
                {
                    string className = "GetNewsNodes";
                    return NodeNotFound(className);
                }
                viewNode = GetNewsNodes.CreateNode(node);
            }
           else
            {
                List<Node> nodes = umbraco.uQuery.GetNodesByType("umbNewsItem").ToList();
                if (nodes == null)
                {
                    string className = "GetNewsNodes";
                    return NodeNotFound(className);
                }
                viewNode = GetNewsNodes.CreateNodes(nodes);
            }

            //    NewsFeedProperties p = new NewsFeedProperties();

            //    List<NewsFeedProperties> nprop = new List<NewsFeedProperties>();

            //    foreach (var vnode in viewNode)
            //        foreach (IProperty prop in vnode.Properties)
            //    {
            //        if (prop.Alias == "image")
            //        {
            //            p.image = prop.Value;
            //        }

            //        if (prop.Alias == "contentTag")
            //        {
            //            p.contentTag = prop.Value;
            //        }

            //        if (prop.Alias == "publisherName")
            //        {
            //            p.publishername = prop.Value;
            //        }

            //        if (prop.Alias == "publishDate")
            //        {
            //            p.publishdate = prop.Value;
            //        }

            //        if (prop.Alias == "bodyText")
            //        {
            //            p.bodyText = prop.Value;
            //        }
                   
            //        p.Alias = prop.Alias;
            //        p.Value = prop.Value;
            //        p.pgid = vnode.Id;
            //        p.name = vnode.Name;
            //        nprop.Add(p);              
            //    }
            //    return JsonResponse(nprop);
            //}

            //List<Node> nodes = umbraco.uQuery.GetNodesByType("umbNewsItem").ToList();            

           


            //List<GetNewsNodes> viewNode = GetNewsNodes.CreateNodes(nodes);


            NewsFeedProperties q = new NewsFeedProperties();

            List<NewsFeedProperties> nprops = new List<NewsFeedProperties>();

            Int64 previousIdval = 0;

            foreach ( var vnode in viewNode)
            { 
               foreach (IProperty prop in vnode.Properties)
               {
                    if (previousIdval < vnode.Id)
                    {
                        q.pgid = vnode.Id;
                    }
                    if (prop.Alias == "image")
                    {
                        q.image = prop.Value;
                    }

                    if (prop.Alias == "contentTag")
                    {
                        q.contentTag = prop.Value;
                    }

                    if (prop.Alias == "publisherName")
                    {
                        q.publishername = prop.Value;
                    }

                    if (prop.Alias == "publishDate")
                    {
                        q.publishdate = prop.Value;
                    }

                    if (prop.Alias == "bodyText")
                    {
                        q.bodyText = prop.Value;
                    }

                    previousIdval = vnode.Id;
                     q.Alias = prop.Alias;
                     q.Value = prop.Value;
                     q.pgid = vnode.Id;
                     q.name = vnode.Name;
               }
                if (q.Alias != "" && q.Value != "")
                {
                    nprops.Add(q);
                    q = new NewsFeedProperties();
                }
            }
            return JsonResponse(nprops);
        }


        [EnableCors(origins: "*", headers: "*", methods: "*")]

        [AcceptVerbs("Options")]
        [HttpGet]
        public HttpResponseMessage GetFaq()
        {           
            List<Node> nodes = umbraco.uQuery.GetNodesByType("faqitem").ToList();

            if (nodes == null) {
                string className = "GetFaqNodes";
                return NodeNotFound(className);
            }


            IEnumerable<GetFaqNodes> viewNode = GetFaqNodes.CreateNodes(nodes);
            List<GetFaqNodes> sortedViewNodeList = viewNode.OrderBy(x => x.Id).ToList<GetFaqNodes>();
            

            FaqProperties q = new FaqProperties();

            List<FaqProperties> nprops = new List<FaqProperties>();
            Int64 previousIdval = 0;
            
            foreach (var vnode in sortedViewNodeList)
            {
                foreach (IProperty prop in vnode.Properties)
                {
                    if (previousIdval < vnode.Id)
                    {
                        q.pgid = vnode.Id;
                    }

                    if (prop.Alias == "question")
                    {
                        q.question = prop.Value;
                    }

                    if (prop.Alias == "answer")
                    {
                        q.Value = prop.Value;
                    }
                    
                    previousIdval = vnode.Id;
                }
                if(q.question != "" && q.Value != "")
                { 
                    nprops.Add(q);
                    q = new FaqProperties();
                }
            }
            return JsonResponse(nprops);
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]

        [AcceptVerbs("Options")]
        [HttpGet]
        public HttpResponseMessage GetLinks()
        {
            List<Node> nodes = umbraco.uQuery.GetNodesByType("importantLinksItem").ToList();

            if (nodes == null) {
                string className = "GetLinkNodes";
                return NodeNotFound(className);
            }


            List<GetLinkNodes> viewNode = GetLinkNodes.CreateNodes(nodes).OrderBy(x => x.Id).ToList<GetLinkNodes>();


            ImpLinksProperties q = new ImpLinksProperties();

            List<ImpLinksProperties> nprops = new List<ImpLinksProperties>();

            Int64 previousIdval = 0;

            foreach (var vnode in viewNode)
            {
                foreach (IProperty prop in vnode.Properties)
                {
                    if (previousIdval < vnode.Id)
                    {
                        q.pgid = vnode.Id;
                    }

                    if (prop.Alias == "link")
                    {
                        q.link = prop.Value;
                    }
                    q.Alias = prop.Alias;
                    q.Value = prop.Value;
                    q.pgid = vnode.Id;
                    q.name = vnode.Name;                
                }
                if(q.link != "")
                {
                    nprops.Add(q);
                    q = new ImpLinksProperties();
                }
            }
            return JsonResponse(nprops);
        }
        private HttpResponseMessage JsonResponse(object obj)
        {
            return new HttpResponseMessage
            {
                Content = JsonContent(obj),
            };
        }

        /// <summary>
        /// Serialize object to json
        /// </summary>
        private StringContent JsonContent(object obj)
        {
            return new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
        }
        private HttpResponseMessage NodeNotFound(string className)
        {   if (className == "GetNewsNodes")
            {
                return JsonResponse(new GetNewsNodes()
                {
                    StatusMessage = new StatusMessage { Success = false, Message = "Node not found" }
                });
            }
            else if (className == "GetFaqNodes")
            {
                return JsonResponse(new GetFaqNodes()
                {
                    StatusMessage = new StatusMessage { Success = false, Message = "Node not found" }
                });
            }
            else
            {
                if (className == "GetFaqNodes") {
                return JsonResponse(new GetLinkNodes()
                {
                    StatusMessage = new StatusMessage { Success = false, Message = "Node not found" }
                });
                }
            }
            return null;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}