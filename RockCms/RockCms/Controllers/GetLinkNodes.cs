﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.cms.businesslogic.web;
using umbraco.interfaces;
using umbraco.NodeFactory;

namespace UmbracoCms.Controllers
{
    public class GetLinkNodes
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public StatusMessage StatusMessage { get; set; }

        public List<IProperty> Properties { get; set; }

        public GetLinkNodes(string Name = null, int Id = 0, List<IProperty> Properties = null)
        {
            this.Name = Name;
            this.Id = Id;
            this.Properties = Properties;
        }

        public static List<GetLinkNodes> CreateNodes(List<Node> nodes)
        {
            List<Node> sortednodes = nodes.Select(n => new { Node = n, newsdate = n.UpdateDate })
                                                 .OrderByDescending(n => n.newsdate).Select(n => n.Node).ToList<Node>();

            List<GetLinkNodes> selectedNodes = new List<GetLinkNodes>();

            foreach (Node node in sortednodes)
            {

                selectedNodes.Add(new GetLinkNodes(node.Name,
                    node.Id,
                    node.PropertiesAsList));
            }
            return selectedNodes;
        }
    }
}
