﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.cms.businesslogic.web;
using umbraco.interfaces;
using umbraco.NodeFactory;


namespace UmbracoCms.Controllers
{
    public class GetNewsNodes
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public StatusMessage StatusMessage { get; set; }
        public List<IProperty> Properties { get; set; }
        public GetNewsNodes(string Name = null, int Id = 0, List<IProperty> Properties = null) {
            this.Name = Name;
            this.Id = Id;
            this.Properties = Properties;
        }     
        public static List<GetNewsNodes> CreateNodes(List<Node> nodes)
        {
            DateTime latestNodeDate = new DateTime();

            List<Node> sortednodes = nodes.Select(n => new { Node = n, newsdate = n.UpdateDate})
                                                 .OrderByDescending(n => n.newsdate).Select(n => n.Node).ToList();

            latestNodeDate = sortednodes[0].UpdateDate;

            List<Node> filteredNodes = sortednodes.Where(n => n.UpdateDate > latestNodeDate.AddDays(-1)).ToList<Node>();

            List<GetNewsNodes> selectedNodes = new List<GetNewsNodes>();

            foreach (Node node in filteredNodes) {
   
                selectedNodes.Add(new GetNewsNodes(node.Name,
                    node.Id,
                    node.PropertiesAsList));
            }
            return selectedNodes;
        }

        public static List<GetNewsNodes> CreateNode(Node node)
        {
            List<GetNewsNodes> selectedNode = new List<GetNewsNodes>();

            selectedNode.Add(new GetNewsNodes(node.Name,
                    node.Id,
                    node.PropertiesAsList));

            return selectedNode;
            
        }
            
    }
}