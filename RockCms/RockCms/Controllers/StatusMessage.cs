﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoCms.Controllers
{
    public class StatusMessage
    {
        /// <summary>
        /// Object that indicates if the node was found or not.
        /// </summary>

        public bool Success { get; set; }
        public string Message { get; set; }
    }
}