﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbraco.interfaces;

namespace UmbracoCms.Controllers
{
    public class FaqProperties : IProperty
    {
        public FaqProperties()
        {
            Alias = "";
            question = "";
            Value = "";
            pgid = 0;
            name = "";
        }

        public string name { get; set; }
        public string Alias { get; set; }

        public int pgid { get; set; }

        public string question { get; set; }

        public string Value { get; set; }
    };
}
